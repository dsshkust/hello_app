class ApplicationController < ActionController::Base
    protect_from_forgery with: :exception

    def hello
        render html:"¡¡¡¡ Hey there! DS here from ruby on rails! Updated the ruby version to 2.4.5, removed rbenv and installed rvm."
    end

    def goodbye
        render html:'Good Bye from Ruby on Rails!! Updated to check that git push heroku works without the master keyword'
    end
end
